package com.aapp.flashoncall.apps_choosing_activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.aapp.flashoncall.R;
import com.aapp.flashoncall.Utils;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class AppsChoosingActivity extends AppCompatActivity{
	
	public static boolean isChecked; //TODO clean 3 state button  behavior or create custom View for it(!!!)
	ImageButton massActions;
	RecyclerView appsRecyclerView;
	MyRVAdapter adapter;
	private Utils utils;
	
	public static boolean containsIgnoreCase( String str , String searchStr ){
		if( str == null ){
			return false;
		}
		
		if( searchStr == null ){
			return true;
		}
		
		final int length = searchStr.length();
		if( length == 0 ){
			return true;
		}
		
		for( int i = str.length() - length ; i >= 0 ; i-- ){
			if( str.regionMatches( true , i , searchStr , 0 , length ) ){
				return true;
			}
		}
		return false;
	}
	
	@Override
	protected void onCreate( Bundle savedInstanceState ){
		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_apps_choosing );
		
		utils = Utils.getInstance( this );
		massActions = findViewById( R.id.check_or_clear_all );
		try{
			utils.getLoadAppsThread().join();
		}catch( InterruptedException e ){
			utils.logToFabric( e.toString() );
		}catch( NullPointerException e ){
			utils.startAppsLoadThread();
			try{
				utils.getLoadAppsThread().join();
			}catch( InterruptedException e1 ){
				e1.printStackTrace();
			}
		}
		
		appsRecyclerView = findViewById( R.id.appsRecyclerView );
		appsRecyclerView.setLayoutManager( new LinearLayoutManager( this ) );
		adapter = new MyRVAdapter( this , massActions );
		appsRecyclerView.setAdapter( adapter );
		
		while( true ){
			String TAG = AppsChoosingActivity.class.getSimpleName();
			Log.i( TAG , String.format( "onCreate enabled: %d" , utils.getEnabledNumber() ) );
			Log.i( TAG , String.format( "onCreate all: %d" , Utils.allAppsNames.size() ) );
			if( utils.getEnabledNumber() == 0 ){
				massActions.setImageResource( R.drawable.ic_check_box_outline_blank_white_24dp );
				isChecked = false;
				break;
			}
			if( utils.getInt( Utils.NOTIFICATIONS_APPS_NUMBER , - 1 ) == - 1 ){
				massActions.setImageResource( R.drawable.ic_check_box_black_24dp );
				isChecked = true;
				break;
			}
			massActions.setImageResource( R.drawable.ic_select_all_white_24dp );
			isChecked = false;
			break;
		}
		
		massActions.setOnClickListener( view -> {
			if( isChecked ){
				utils.disableAll();
				massActions.setImageResource( R.drawable.ic_check_box_outline_blank_white_24dp );
			}else{
				utils.enableAll();
				massActions.setImageResource( R.drawable.ic_check_box_black_24dp );
			}
			isChecked = ! isChecked;
			adapter.notifyDataSetChanged();
		} );
		SearchView searchAppsView = findViewById( R.id.search_apps );
		searchAppsView.setOnSearchClickListener( view -> {
			TextView activityLabel = findViewById( R.id.toolbar_label );
			activityLabel.setVisibility( View.INVISIBLE );
		} );
		searchAppsView.setOnCloseListener( () -> {
			TextView activityLabel = findViewById( R.id.toolbar_label );
			activityLabel.setVisibility( View.VISIBLE );
			return false;
		} );
		searchAppsView.setOnQueryTextListener( new SearchView.OnQueryTextListener(){
			
			@Override
			public boolean onQueryTextSubmit( String query ){
				return false;
			}
			
			@Override
			public boolean onQueryTextChange( String newText ){
				MyRVAdapter.adapterAppsList = new ArrayList<>( Utils.allAppsInfo );
				for( int i = 0 ; i < MyRVAdapter.adapterAppsList.size() ; i++ ){
					if( ! containsIgnoreCase( MyRVAdapter.adapterAppsList.get( i ).loadLabel( utils.getPackageManager() ).toString() , newText ) ){
						MyRVAdapter.adapterAppsList.remove( i );
						i--;
					}
				}
				adapter.notifyDataSetChanged();
				return false;
			}
		} );
	}
}
