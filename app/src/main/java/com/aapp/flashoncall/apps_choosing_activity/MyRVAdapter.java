package com.aapp.flashoncall.apps_choosing_activity;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.aapp.flashoncall.R;
import com.aapp.flashoncall.Utils;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyRVAdapter extends RecyclerView.Adapter< MyRVAdapter.MyRVHolder >{
	
	static ArrayList< ApplicationInfo > adapterAppsList = new ArrayList<>( Utils.allAppsInfo );
	private ImageButton massAction;
	private Utils utils;
	
	MyRVAdapter( Context context , ImageButton massAction ){
		utils = Utils.getInstance( context );
		this.massAction = massAction;
	}
	
	@NonNull
	@Override
	public MyRVHolder onCreateViewHolder( @NonNull ViewGroup parent , int viewType ){
		View view = LayoutInflater.from( parent.getContext() ).inflate( R.layout.apps_list_item , parent , false );
		return new MyRVHolder( view );
	}
	
	@Override
	public void onBindViewHolder( @NonNull MyRVHolder holder , int position ){
		holder.bind( adapterAppsList.get( position ) );
	}
	
	@Override
	public int getItemCount(){
		return adapterAppsList.size();
	}
	
	private void enable( ImageButton massAction , String appName ){
		utils.enable( appName );
		if( utils.getEnabledNumber() == Utils.allAppsNames.size() ){
			massAction.setImageResource( R.drawable.ic_check_box_black_24dp );
			AppsChoosingActivity.isChecked = true;
		}else{
			massAction.setImageResource( R.drawable.ic_select_all_white_24dp );
			AppsChoosingActivity.isChecked = false;
		}
	}
	
	private void disable( ImageButton massAction , String appName ){
		utils.disable( appName );
		if( utils.getEnabledNumber() == 0 ){
			massAction.setImageResource( R.drawable.ic_check_box_outline_blank_white_24dp );
		}else{
			massAction.setImageResource( R.drawable.ic_select_all_white_24dp );
		}
		AppsChoosingActivity.isChecked = false;
	}
	class MyRVHolder extends RecyclerView.ViewHolder{
		
		CheckBox isEnabledView;
		ImageView appImageView;
		TextView appNameView;
		
		MyRVHolder( @NonNull View itemView ){
			super( itemView );
			isEnabledView = itemView.findViewById( R.id.isNotificationOn );
			appImageView = itemView.findViewById( R.id.appIcon );
			appNameView = itemView.findViewById( R.id.appName );
		}
		
		void bind( ApplicationInfo app ){
			final String appName = app.loadLabel( utils.getPackageManager() ).toString();
			appNameView.setText( appName );
			appImageView.setImageDrawable( app.loadIcon( utils.getPackageManager() ) );
			isEnabledView.setOnCheckedChangeListener( null );
			isEnabledView.setChecked( utils.isEnabled( appName ) );
			isEnabledView.setOnCheckedChangeListener( ( ( buttonView , isChecked ) -> {
				if( isChecked ){
					enable( massAction , appName );
				}else{
					disable( massAction , appName );
				}
			} ) );
		}
	}
}
