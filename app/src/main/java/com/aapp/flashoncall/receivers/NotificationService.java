package com.aapp.flashoncall.receivers;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.IBinder;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import com.aapp.flashoncall.Utils;
import com.aapp.flashoncall.flashers.NotificationFlasher;
import com.crashlytics.android.Crashlytics;

public class NotificationService extends NotificationListenerService{
	private static final String TAG = NotificationService.class.getSimpleName();
	Utils utils;
	
	@Override
	public void onCreate(){
		super.onCreate();
		utils = Utils.getInstance( this );
		utils.startAppsLoadThread();
	}
	
	@Override
	public boolean onUnbind( Intent intent ){
		return super.onUnbind( intent );
	}
	
	@Override
	public void onNotificationPosted( StatusBarNotification sbn ){
		if( Utils.allAppsNames == null ){
			utils.logToFabric( "all apps names == null" , TAG );
		}
		Log.i( TAG , "onNotificationPosted: " );
		boolean isRingingModeOK = false;
		AudioManager audioManager = (AudioManager) this.getSystemService( Context.AUDIO_SERVICE );
		if( audioManager == null ){
			return;
		}
		switch( audioManager.getRingerMode() ){
			case AudioManager.RINGER_MODE_NORMAL:
				isRingingModeOK = utils.getBoolean( Utils.NORMAL_MODE , true );
				break;
			case AudioManager.RINGER_MODE_VIBRATE:
				isRingingModeOK = utils.getBoolean( Utils.VIBRATE_MODE , true );
				break;
			case AudioManager.RINGER_MODE_SILENT:
				isRingingModeOK = utils.getBoolean( Utils.SILENT_MODE , false );
				break;
		}
		
		boolean contains = false;
		try{
			contains = utils.isEnabled( utils.getPackageManager().getApplicationInfo( sbn.getPackageName() , PackageManager.GET_META_DATA ).loadLabel( utils.getPackageManager() ).toString() );
		}catch( PackageManager.NameNotFoundException e ){
			Crashlytics.logException( e );
		}
		BatteryManager batteryManager = (BatteryManager) this.getSystemService( Context.BATTERY_SERVICE );
		boolean isBatteryOk = false;
		try{
			isBatteryOk = utils.getInt( Utils.BATTERY_LEVEL , 25 ) <= batteryManager.getIntProperty( BatteryManager.BATTERY_PROPERTY_CAPACITY );
		}catch( Exception e ){
			Crashlytics.logException( e );
		}
		boolean isEnabled = utils.getBoolean( Utils.NOTIFICATIONS , false );
		
		if( sbn.isClearable() && isBatteryOk && contains && isEnabled && isRingingModeOK ){
			new NotificationFlasher( Utils.ON_TIME , Utils.OFF_TIME , Utils.FLASH_NUMBER , this.getApplicationContext() ).start();
		}
		
		boolean isSMS = false;
		try{
			isSMS = sbn.getPackageName().equals( new Intent( Intent.ACTION_VIEW , Uri.parse( "sms: +111111" ) ).putExtra( "sms_body" , "11111" ).resolveActivity( utils.getPackageManager() ).getPackageName() );
			//resolveActivity can return null if activity doesn't exist
		}catch( NullPointerException e ){
			utils.logToFabric( e.getMessage() , TAG );
		}
		boolean isSMSEnabled = utils.getBoolean( Utils.SMS , false );
		if( isSMS && isSMSEnabled && isBatteryOk && isRingingModeOK ){
			new NotificationFlasher( Utils.ON_TIME , Utils.OFF_TIME , Utils.FLASH_NUMBER , this.getApplicationContext() ).start();
		}
	}
	
	@Override
	public void onNotificationRemoved( StatusBarNotification sbn ){
	}
	
	@Override
	public void onListenerConnected(){
		super.onListenerConnected();
		try{
			utils.getLoadAppsThread().join();
		}catch( InterruptedException e ){
			utils.logToFabric( e.getMessage() , TAG );
		}catch( NullPointerException e ){
			utils.logToFabric( e.getMessage() , TAG );
			utils.startAppsLoadThread();
		}
	}
	
	@Override
	public void onListenerDisconnected(){
		super.onListenerDisconnected();
	}
	
	@Override
	public IBinder onBind( Intent intent ){
		return super.onBind( intent );
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
	}
}
