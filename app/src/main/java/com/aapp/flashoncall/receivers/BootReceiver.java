package com.aapp.flashoncall.receivers;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.aapp.flashoncall.Utils;
import com.crashlytics.android.Crashlytics;

public class BootReceiver extends BroadcastReceiver{
	public static final int ID = 1111;
	private static final String TAG = BootReceiver.class.getSimpleName();
	
	@Override
	public void onReceive( Context context , Intent intent ){
		Log.i( TAG , "onReceive: init started" );
		Utils.getInstance( context );
		ComponentName componentName = new ComponentName( context , MyJobService.class );
		JobInfo info = new JobInfo.Builder( ID , componentName ).setOverrideDeadline( 1000 ).build();
		JobScheduler scheduler = ( (JobScheduler) context.getSystemService( Context.JOB_SCHEDULER_SERVICE ) );
		try{
			scheduler.schedule( info );
		}catch( Exception e ){
			Crashlytics.logException( e );
			e.printStackTrace();
		}
	}
}
