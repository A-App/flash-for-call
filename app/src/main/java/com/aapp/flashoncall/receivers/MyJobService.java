package com.aapp.flashoncall.receivers;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.IntentFilter;
import android.os.Build;
import android.util.Log;

import com.aapp.flashoncall.Utils;

public class MyJobService extends JobService{
	
	private static final String TAG = MyJobService.class.getSimpleName();
	
	@Override
	public boolean onStartJob( JobParameters jobParameters ){
		Log.i( TAG , "onStartJob: job is started" );
		Utils.getInstance( this );
		if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ){
			CallReceiver callReceiver = new CallReceiver();
			IntentFilter callIntentFilter = new IntentFilter( "android.intent.action.PHONE_STATE" );
			registerReceiver( callReceiver , callIntentFilter );
		}
//		try{
//		}catch( Exception e ){
//			utils.logToFabric( e.getMessage() , TAG );
//		}
//		try{
//			utils.getLoadAppsThread().join();
//		}catch( InterruptedException e ){
//			utils.logToFabric( e.getMessage() );
//		}catch( NullPointerException e ){
//			utils.logToFabric( e.getMessage() );
//		}
		Log.i( TAG , "onStartJob: load finished" );
		return false;
	}
	
	@Override
	public boolean onStopJob( JobParameters jobParameters ){
		return false;
	}
}
