package com.aapp.flashoncall.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.BatteryManager;
import android.preference.PreferenceManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import com.aapp.flashoncall.Utils;
import com.aapp.flashoncall.flashers.CallFlasher;
import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

import static com.aapp.flashoncall.Utils.RINGING;

public class CallReceiver extends BroadcastReceiver{
	
	private static final String TAG = CallReceiver.class.getSimpleName();
	public static String callState = "IDLE";
	static boolean isCallsActive, isRingingModeOK;
	
	@Override
	public void onReceive( Context context , Intent intent ){
		Fabric.with( context , new Crashlytics() );
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences( context );

//		if( ! preferences.getBoolean( MainActivity.IS_SYSTEM_OK , true ) ){
//			return;
//		}
		
		isCallsActive = preferences.getBoolean( Utils.CALLS , true );
		
		BatteryManager batteryManager = (BatteryManager) context.getSystemService( Context.BATTERY_SERVICE );
		boolean isBatteryOK;
		if( batteryManager == null ){
			Crashlytics.log( "BatteryManager == null" );
			return;
		}
		isBatteryOK = preferences.getInt( Utils.BATTERY_LEVEL , 25 ) <= batteryManager.getIntProperty( BatteryManager.BATTERY_PROPERTY_CAPACITY );
		
		AudioManager audioManager = (AudioManager) context.getSystemService( Context.AUDIO_SERVICE );
		if( audioManager == null ){
			Crashlytics.log( "AudioManager == null " );
			return;
		}
		switch( audioManager.getRingerMode() ){
			case AudioManager.RINGER_MODE_NORMAL:
				isRingingModeOK = preferences.getBoolean( Utils.NORMAL_MODE , true );
				break;
			case AudioManager.RINGER_MODE_VIBRATE:
				isRingingModeOK = preferences.getBoolean( Utils.VIBRATE_MODE , true );
				break;
			case AudioManager.RINGER_MODE_SILENT:
				isRingingModeOK = preferences.getBoolean( Utils.SILENT_MODE , false );
				break;
		}
		
		MyPhoneStateListener listener = new MyPhoneStateListener( context );
		TelephonyManager telephony = (TelephonyManager) context.getSystemService( Context.TELEPHONY_SERVICE );
		if( isCallsActive && isRingingModeOK && isBatteryOK ){
			try{
				telephony.listen( listener , PhoneStateListener.LISTEN_CALL_STATE );
			}catch( Exception e ){
				e.printStackTrace();
				Crashlytics.logException( e );
			}
		}
	}
	
	static class MyPhoneStateListener extends PhoneStateListener{
		
		private static final String IDLE = "IDLE";
		private static final String OFFHOOK = "OFFHOOK";
		private Context context;
		
		MyPhoneStateListener( Context con ){
			context = con;
		}
		
		@Override
		public void onCallStateChanged( int state , String incomingNumber ){
			switch( state ){
				case TelephonyManager.CALL_STATE_IDLE:
					callState = IDLE;
					break;
				case TelephonyManager.CALL_STATE_OFFHOOK:
					callState = OFFHOOK;
					break;
				case TelephonyManager.CALL_STATE_RINGING:
					callState = RINGING;
					new CallFlasher( context ).start();
					break;
			}
			super.onCallStateChanged( state , incomingNumber );
		}
	}
}
