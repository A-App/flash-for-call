package com.aapp.flashoncall;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import io.fabric.sdk.android.Fabric;

public final class Utils{
	//singleton util class
	
	//---------------------preference keys-------------------
	public static final String CALLS = "calls";
	public static final String SMS = "sms";
	public static final String NOTIFICATIONS = "notifications";
	public static final String NOTIFICATIONS_APPS_NUMBER = "number of apps with notifications enabled";
	public static final String ON_TIME = "on time";
	public static final String OFF_TIME = "off time";
	public static final String FLASH_NUMBER = "number of flashes";
	public static final String IS_FRONT_FLASH_EXIST = "front flash availability";
	public static final String IS_FIRST_LAUNCH = "first launch"; //TODO tests
	public static final String BATTERY_LEVEL = "battery";
	public static final String NORMAL_MODE = "normal mode";
	public static final String VIBRATE_MODE = "vibrate mode";
	public static final String SILENT_MODE = "silent mode";
	public static final String RINGING = "RINGING";
	public static final String MAIN_FLASH_ACTIVE = "is main flash active";
	public static final String FRONT_FLASH_ACTIVE = "is front flash active";
	//---------------------int constants-----------------------------------------------
	public static final int ADDITIONAL_DELAY_VALUE = 50;
	public static final int ADDITIONAL_NUMBER_VALUE = 1;
	public static final int DEFAULT_DELAY_VALUE = 250;
	public static final int DEFAULT_FLASH_NUMBER_VALUE = 5;
	private static final String ENABLED_APPS = "enable apps";
	private static final String TAG = Utils.class.getSimpleName();
	public static List< String > allAppsNames;
	public static List< ApplicationInfo > allAppsInfo;
	//--------------------------------------------------
	private static Utils instance;
	//---------------collections for notifications apps choosing---------
	private List< String > enabledApps;
	private SharedPreferences preferences;
	private PackageManager packageManager;
	private CameraManager cameraManager;
	private Thread loadAppsThread;
	
	//TODO enable front flash
	
	private Utils( Context context ){
		//global debug tools init
		FirebaseAnalytics.getInstance( context );
		Fabric.with( context , new Crashlytics() );
		
		//local tools init
		packageManager = context.getPackageManager();
		preferences = PreferenceManager.getDefaultSharedPreferences( context );
		cameraManager = (CameraManager) context.getSystemService( Context.CAMERA_SERVICE );
		
		//variables init
		
	}
	
	public static Utils getInstance( @NonNull Context context ){
		if( instance == null ){
			instance = new Utils( context );
		}
		return instance;
	}
	
	//----------apps collections work-------------------------------------------
	private void uploadEnabledChanges(){
		int size = enabledApps.size() == allAppsNames.size() ? - 1 : enabledApps.size();
		putInt( NOTIFICATIONS_APPS_NUMBER , size );
		putStringSet( ENABLED_APPS , enabledApps );
	}
	
	public void enable( String name ){
		if( enabledApps == null ){
			enabledApps = new ArrayList<>();
		}
		if( enabledApps.contains( name ) ){
			return;
		}
		enabledApps.add( name );
		uploadEnabledChanges();
	}
	
	public void disable( String name ){
		if( enabledApps != null ){
			enabledApps.remove( name );
		}
		uploadEnabledChanges();
	}
	
	public void enableAll(){
		enabledApps = new ArrayList<>( allAppsNames );
		uploadEnabledChanges();
	}
	
	public void disableAll(){
		enabledApps = new ArrayList<>();
		uploadEnabledChanges();
	}
	
	public int getEnabledNumber(){
		return enabledApps == null ? 0 : enabledApps.size();
	}
	
	public boolean isEnabled( String name ){
		if( allAppsNames == null && enabledApps == null ) return false;
		if( enabledApps == null ) enabledApps = new ArrayList<>( getStringSet( ENABLED_APPS , allAppsNames ) );
		return enabledApps.contains( name );
	}
	
	private void getAllApps(){
		Log.i( TAG , "getAllApps: started" );
		//code for allAppsThread
		
		List< ApplicationInfo > rawApps = packageManager.getInstalledApplications( PackageManager.GET_META_DATA );
		for( int i = 0 ; i < rawApps.size() ; i++ ){
			if( packageManager.getLaunchIntentForPackage( rawApps.get( i ).packageName ) == null ){
				rawApps.remove( i );
				i--;
			}
		}
		ApplicationInfo sms = null, calls = null;
		try{
			sms = packageManager.getApplicationInfo( new Intent( Intent.ACTION_VIEW , Uri.parse( "sms: 1" ) ).resolveActivity( packageManager ).getPackageName() , PackageManager.GET_META_DATA );
			calls = packageManager.getApplicationInfo( new Intent( Intent.ACTION_DIAL ).resolveActivity( packageManager ).getPackageName() , PackageManager.GET_META_DATA );
		}catch( PackageManager.NameNotFoundException e ){
			e.printStackTrace();
		}
		for( int i = 0 ; sms != null && i < rawApps.size() ; i++ ){
			if( rawApps.get( i ).packageName.equals( sms.packageName ) ){
				rawApps.remove( i );
				break;
			}
		}
		for( int i = 0 ; calls != null && i < rawApps.size() ; i++ ){
			if( rawApps.get( i ).packageName.equals( calls.packageName ) ){
				rawApps.remove( i );
				break;
			}
		}
		TreeMap< String, ApplicationInfo > map = new TreeMap<>();
		for( ApplicationInfo app : rawApps ){
			map.put( app.loadLabel( packageManager ).toString() , app );
		}
		List< String > appsNames = new ArrayList<>();
		List< ApplicationInfo > appsInfo = new ArrayList<>();
		for( Map.Entry< String, ApplicationInfo > entry : map.entrySet() ){
			appsNames.add( entry.getKey() );
			appsInfo.add( entry.getValue() );
		}
		Log.i( TAG , "getAllApps: apps loaded" );
		allAppsInfo = appsInfo;
		allAppsNames = appsNames;
		enabledApps = new ArrayList<>( getStringSet( ENABLED_APPS , allAppsNames ) );
	}
	
	public void startAppsLoadThread(){
		if( loadAppsThread != null ){
			return;
		}
		loadAppsThread = new Thread( this::getAllApps );
		loadAppsThread.start();
	}
	
	public Thread getLoadAppsThread(){
		return loadAppsThread;
	}
	
	//-------------------------tools getters----------------------------------
	
	public PackageManager getPackageManager(){
		return packageManager;
	}
	
	//-------------DEBUG-------------------------------------------------------------
	
	public void logToFabric( String message , String TAG ){
		Crashlytics.logException( new Exception( message ) );
		Log.i( TAG , message );
	}
	
	public void logToFabric( String message ){
		logToFabric( message , TAG );
	}
	
	//-----------------preferences-----------------------------------------
	
	public void putInt( String key , int value ){
		preferences.edit().putInt( key , value ).apply();
	}
	
	public int getInt( @NonNull String key , int defValue ){
		return preferences.getInt( key , defValue );
	}
	
	public void putBoolean( String key , boolean value ){
		preferences.edit().putBoolean( key , value ).apply();
	}
	
	public boolean getBoolean( @NonNull String key , boolean defValue ){
		return preferences.getBoolean( key , defValue );
	}
	
	private void putStringSet( @NonNull String key , Collection< String > value ){
		preferences.edit().putStringSet( key , new HashSet<>( value ) ).apply();
	}
	
	private Set< String > getStringSet( @NonNull String key , Collection< String > defValue ){
		return preferences.getStringSet( key , new HashSet<>( defValue ) );
	}
	
	//---------------------------------------permissions------------------------------------------------------------------------------------
	
	public boolean isCallsDenied( Context context ){
		return ContextCompat.checkSelfPermission( context , Manifest.permission.READ_PHONE_STATE ) == PackageManager.PERMISSION_DENIED;
	}
	
	public boolean isSmsPermitted( Context context ){
		return isNotificationsPermitted( context );
	}
	
	public boolean isNotificationsPermitted( Context context ){
		String setting = Settings.Secure.getString( context.getContentResolver() , "enabled_notification_listeners" );
		return setting != null && setting.contains( context.getPackageName() );
	}
	
	public boolean isFlashOk(){
		//both tested
		boolean ans = false;
		boolean isFrontFlash = false;
		if( Build.VERSION.SDK_INT < Build.VERSION_CODES.M ){
			int cameras = Camera.getNumberOfCameras();
			
			Camera camera = Camera.open();
			List< String > flashModes = camera.getParameters().getSupportedFlashModes();
			ans = flashModes != null && flashModes.contains( Camera.Parameters.FLASH_MODE_ON );
			camera.release();
			camera = cameras > 1 ? Camera.open( 1 ) : null;
			flashModes = camera != null ? camera.getParameters().getSupportedFlashModes() : null;
			isFrontFlash = flashModes != null && flashModes.contains( Camera.Parameters.FLASH_MODE_ON );
			if( camera != null ) camera.release();
		}else{
			try{
				String[] cameraIDs = cameraManager.getCameraIdList();
				ans = cameraIDs.length > 0 && cameraManager.getCameraCharacteristics( cameraIDs[0] ).get( CameraCharacteristics.FLASH_INFO_AVAILABLE );
				isFrontFlash = cameraIDs.length > 1 && cameraManager.getCameraCharacteristics( cameraIDs[1] ).get( CameraCharacteristics.FLASH_INFO_AVAILABLE );
			}catch( CameraAccessException e ){
				e.printStackTrace();
			}catch( NullPointerException e ){
				e.printStackTrace();
			}
		}
		putBoolean( IS_FRONT_FLASH_EXIST , isFrontFlash );
		return ans;
	}
}
