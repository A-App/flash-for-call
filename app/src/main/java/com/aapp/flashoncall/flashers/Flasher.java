package com.aapp.flashoncall.flashers;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

public abstract class Flasher extends Thread{
	private static final String TAG = Flasher.class.getSimpleName();
	int numberOfFlashes = 3;
	private int onDuration;
	private int offDuration;
	private Context context;
	
	//TODO utils this one
	
	Flasher( int onDuration , int offDuration , Context context ){
		this.onDuration = onDuration;
		this.offDuration = offDuration;
		this.context = context;
		this.numberOfFlashes = 3;
	}
	
	Flasher( String ON_KEY , String OFF_KEY , Context context ){
		super();
		this.context = context;
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences( context );
		onDuration = preferences.getInt( ON_KEY , 250 );
		offDuration = preferences.getInt( OFF_KEY , 250 );
	}
	
	Flasher( String ON_KEY , String OFF_KEY , String NUMBER_KEY , Context context ){
		super();
		this.context = context;
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences( context );
		onDuration = preferences.getInt( ON_KEY , 250 );
		offDuration = preferences.getInt( OFF_KEY , 250 );
		numberOfFlashes = preferences.getInt( NUMBER_KEY , 1 );
	}
	
	abstract void flashes();
	
	void flash(){
		Log.i( TAG , "flashed" );
		// Made for testing on emulators without flash//camera//both
		try{
			if( Build.VERSION.SDK_INT < 23 ){
				Camera flash = null;
				Camera.Parameters parametersOn = null, parametersOff = null;
				flash = Camera.open();
				if( flash == null ){
					Crashlytics.log( "Flasher: flash == null" );
					return;
				}
				parametersOn = flash.getParameters();
				parametersOff = flash.getParameters();
				parametersOn.setFlashMode( Camera.Parameters.FLASH_MODE_TORCH );
				parametersOff.setFlashMode( Camera.Parameters.FLASH_MODE_OFF );
				
				flash.setPreviewTexture( new SurfaceTexture( 0 ) );
				flash.startPreview();
				flash.setParameters( parametersOn );
				Thread.sleep( onDuration );
				flash.stopPreview();
				flash.setParameters( parametersOff );
				flash.release();
			}else{
				CameraManager cameraManager = (CameraManager) context.getSystemService( Context.CAMERA_SERVICE );
				if( cameraManager == null ){
					Crashlytics.log( "Flasher: cameraManager == null" );
					return;
				}
				String cameraID = null;
				cameraID = cameraManager.getCameraIdList()[0];
				if( cameraID == null ){
					Crashlytics.log( "Flasher: cameraID == null" );
					return;
				}
				cameraManager.setTorchMode( cameraID , true );
				Thread.sleep( onDuration );
				cameraManager.setTorchMode( cameraID , false );
				Thread.sleep( offDuration );
			}
		}catch( Exception e ){
			Crashlytics.logException( e );
		}
		Log.i( TAG , "flash: end" );
	}
	
	@Override
	public void run(){
		super.run();
		flashes();
	}
}

