package com.aapp.flashoncall.flashers;

import android.content.Context;
import android.util.Log;

public class NotificationFlasher extends Flasher{
	
	public NotificationFlasher( String ON_KEY , String OFF_KEY , String NUMBER_KEY , Context context ){
		super( ON_KEY , OFF_KEY , NUMBER_KEY , context );
	}
	
	@Override
	void flashes(){
		for( int i = 0 ; i < numberOfFlashes ; i++ ){
			Log.i( NotificationFlasher.class.getSimpleName() , "flashes: flash" );
			flash();
		}
	}
}
