package com.aapp.flashoncall.flashers;

import android.content.Context;
import android.util.Log;

import com.aapp.flashoncall.Utils;
import com.aapp.flashoncall.receivers.CallReceiver;

public class CallFlasher extends Flasher{
	public CallFlasher( Context context ){
		super( Utils.ON_TIME , Utils.OFF_TIME , context );
	}
	
	@Override
	void flashes(){
		Log.i( CallFlasher.class.getSimpleName() , "flashes()" );
		
		while( CallReceiver.callState.equals( Utils.RINGING ) ){
			Log.i( CallFlasher.class.getSimpleName() , "flashes: flash" );
			flash();
		}
	}
}
//TODO Listener pattern