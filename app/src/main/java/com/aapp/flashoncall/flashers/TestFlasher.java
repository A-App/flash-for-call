package com.aapp.flashoncall.flashers;

import android.content.Context;

public class TestFlasher extends Flasher{
	private int number;
/*	public TestFlasher( int onDuration , int offDuration ){
		super( onDuration , offDuration );
	}*/
	
	public TestFlasher( int onDuration , int offDuration , Context context ){
		super( onDuration , offDuration , context );
		number = 3;
	}
	
	public TestFlasher( int onDuration , int offDuration , int number , Context context ){
		super( onDuration , offDuration , context );
		this.number = number;
	}

//	public static TestFlasher getInstance( int onDuration , int offDuration ){
//		if( instance == null ){
//			synchronized( TestFlasher.class ){
//				if( instance == null ){
//					instance = new TestFlasher( onDuration , offDuration );
//				}
//			}
//		}
//		return instance;
//	}
	
	@Override
	void flashes(){
		for( int i = 0 ; i < number ; i++ ){
			flash();
		}
	}
}
