package com.aapp.flashoncall.main_activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.aapp.flashoncall.BuildConfig;
import com.aapp.flashoncall.R;
import com.aapp.flashoncall.Utils;
import com.aapp.flashoncall.apps_choosing_activity.AppsChoosingActivity;
import com.aapp.flashoncall.flashers.TestFlasher;
import com.aapp.flashoncall.receivers.BootReceiver;
import com.aapp.flashoncall.receivers.MyJobService;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

public class MainActivity extends AppCompatActivity{
	
	private static final String TAG = MainActivity.class.getSimpleName();
	//	static Thread appsListLoad;
//	private static boolean askCallPermission = false;
	Switch callSwitch, smsSwitch, notificationSwitch;
	private Utils utils;
	private AlertDialog callPermissionDialog, notificationPermissionDialog;
	private TextView appsListInfo, workingModesInfo;
	private boolean isFromCallPermissionDialog = false;
	
	//-------------------------------------Activity lifecycle-------------------------------------------------------------
	
	@Override
	protected void onCreate( Bundle savedInstanceState ){
		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_main );
		
		utils = Utils.getInstance( this );
		
		utils.startAppsLoadThread();
		
		//some actions which will done on first launch
		if( utils.getBoolean( Utils.IS_FIRST_LAUNCH , false ) ){
			
			//start call receiver on Android 6.0(M)+
			ComponentName componentName = new ComponentName( this , MyJobService.class );
			JobInfo info = new JobInfo.Builder( BootReceiver.ID , componentName ).setOverrideDeadline( 1000 ).build();
			JobScheduler scheduler = ( (JobScheduler) getSystemService( Context.JOB_SCHEDULER_SERVICE ) );
			scheduler.schedule( info );
			
			utils.putBoolean( Utils.IS_FIRST_LAUNCH , true );
		}
		
		callSwitch = findViewById( R.id.call );
		callSwitch.setOnCheckedChangeListener( ( ( buttonView , isChecked ) -> callSwitchTurned( isChecked ) ) );
		callSwitch.setChecked( utils.getBoolean( Utils.CALLS , true ) );
		
		smsSwitch = findViewById( R.id.sms );
		smsSwitch.setOnCheckedChangeListener( ( buttonView , isChecked ) -> smsSwitchTurned( isChecked ) );
		smsSwitch.setChecked( utils.getBoolean( Utils.SMS , false ) );
		
		notificationSwitch = findViewById( R.id.notification );
		notificationSwitch.setChecked( utils.getBoolean( Utils.NOTIFICATIONS , false ) && utils.isNotificationsPermitted( this ) );
		notificationSwitch.setOnCheckedChangeListener( ( buttonView , isChecked ) -> notificationsSwitchTurned( isChecked ) );
		
		//check availability of main camera flash , front flash(in preferences)
		if( ! utils.isFlashOk() ){
			//TODO all functions off
			/*some work for stopping app such as:
			 * unregisterReceiver
			 * calls , sms , missed , notificationSwitch off
			 *
			 */
			if( callPermissionDialog != null ){
				callPermissionDialog.cancel();
			}
			if( notificationPermissionDialog != null ){
				notificationPermissionDialog.cancel();
			}
//			TODO uncomment next lines in release
			callSwitch.setChecked( false );
			smsSwitch.setChecked( false );
			notificationSwitch.setChecked( false );
//			TODO shutdown missed
			showFlashAlert();
		}
		
		appsListInfo = findViewById( R.id.apps_list_info );
		workingModesInfo = findViewById( R.id.working_modes_info );
		updateWorkingModesInfoText();
		updateAppsNumberText();
		
		( (TextView) findViewById( R.id.version ) ).setText( BuildConfig.VERSION_NAME );
	}
	
	@Override
	protected void onPause(){
		super.onPause();
	}
	
	@Override
	protected void onResume(){
		updateAppsNumberText();
		if( isFromCallPermissionDialog ){
			callSwitch.setChecked( callSwitch.isChecked() && ! utils.isCallsDenied( this ) );
		}
		super.onResume();
	}
	
	@Override
	protected void onRestart(){
		notificationSwitch.setChecked( notificationSwitch.isChecked() && utils.isNotificationsPermitted( this ) );
		smsSwitch.setChecked( smsSwitch.isChecked() && utils.isSmsPermitted( this ) );
		super.onRestart();
	}
	
	
	//------------------Menu items on click-------------------------------------------------------------------------------
	
	private void callSwitchTurned( boolean isChecked ){
		if( isChecked && utils.isCallsDenied( this ) ){
			showCallPermissionDialog();
		}
		utils.putBoolean( Utils.CALLS , isChecked );
	}
	
	private void smsSwitchTurned( boolean isChecked ){
		if( isChecked && ! utils.isSmsPermitted( this ) ){
			showNotificationsPermissionDialog();
		}
		utils.putBoolean( Utils.SMS , isChecked );
	}
	
	private void notificationsSwitchTurned( boolean isChecked ){
		if( isChecked && ! utils.isNotificationsPermitted( this ) ){
			showNotificationsPermissionDialog();
		}
		utils.putBoolean( Utils.NOTIFICATIONS , isChecked );
	}
	
	public void showAppChoosingActivity( View view ){
		startActivity( new Intent( this , AppsChoosingActivity.class ) );
	}
	
	public void workingModeOnClick( View view ){
		AlertDialog.Builder builder = new AlertDialog.Builder( this );
		builder.setTitle( R.string.working_modes_settings );
		//--------------------------------------View setup---------------------------------
		View dialogView = getLayoutInflater().inflate( R.layout.working_modes_dialog , null );
		TextView batteryLevelText = dialogView.findViewById( R.id.battery_level_indicator_text );
		SeekBar batteryLevelSeekBar = dialogView.findViewById( R.id.battery_level_seekBar );
		batteryLevelSeekBar.setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener(){
			@Override
			public void onProgressChanged( SeekBar seekBar , int progress , boolean fromUser ){
				batteryLevelText.setText( getString( R.string.percents , progress ) );
			}
			
			@Override
			public void onStartTrackingTouch( SeekBar seekBar ){
			
			}
			
			@Override
			public void onStopTrackingTouch( SeekBar seekBar ){
			
			}
		} );
		batteryLevelSeekBar.setProgress( utils.getInt( Utils.BATTERY_LEVEL , 25 ) );
		
		CheckBox normalMode = dialogView.findViewById( R.id.normal_mode_checkbox );
		normalMode.setChecked( utils.getBoolean( Utils.NORMAL_MODE , true ) );
		CheckBox vibrateMode = dialogView.findViewById( R.id.vibrate_mode_checkbox );
		vibrateMode.setChecked( utils.getBoolean( Utils.VIBRATE_MODE , true ) );
		CheckBox silentMode = dialogView.findViewById( R.id.silent_mode_checkbox );
		silentMode.setChecked( utils.getBoolean( Utils.SILENT_MODE , false ) );
		
		//-------------------------------------------------------------------------------------------
		builder.setView( dialogView );
		builder.setPositiveButton( R.string.save , ( dialog , which ) -> {
			utils.putInt( Utils.BATTERY_LEVEL , batteryLevelSeekBar.getProgress() );
			utils.putBoolean( Utils.NORMAL_MODE , normalMode.isChecked() );
			utils.putBoolean( Utils.VIBRATE_MODE , vibrateMode.isChecked() );
			utils.putBoolean( Utils.SILENT_MODE , silentMode.isChecked() );
			//TODO save on off times
			updateWorkingModesInfoText();
		} );
		builder.setNegativeButton( R.string.cancel , ( dialog , which ) -> {
			dialog.cancel();
		} );
		builder.create().show();
	}
	
	public void flashSettingsOnClick( View view ){
		AlertDialog.Builder builder = new AlertDialog.Builder( this );
		builder.setTitle( R.string.flash_settings );
		//-----------------View setup-------------------------------------------------------------
		View dialogView = getLayoutInflater().inflate( R.layout.flash_settings_dialog , null );
		if( ! utils.getBoolean( Utils.IS_FRONT_FLASH_EXIST , false ) ){
			dialogView.findViewById( R.id.front_flash_layout ).setVisibility( View.GONE );
		}
		
		CheckBox mainFlash = dialogView.findViewById( R.id.main_flash_checkbox );
		mainFlash.setChecked( utils.getBoolean( Utils.MAIN_FLASH_ACTIVE , true ) );
		CheckBox frontFlash = dialogView.findViewById( R.id.front_flash_checkbox );
		frontFlash.setChecked( utils.getBoolean( Utils.FRONT_FLASH_ACTIVE , false ) );
		
		TextView onDurationIndicator = dialogView.findViewById( R.id.on_duration_indicator_text );
		TextView offDurationIndicator = dialogView.findViewById( R.id.off_duration_indicator_text );
		TextView numberIndicator = dialogView.findViewById( R.id.flash_number_indicator_text );
		
		SeekBar onDurationSeekBar = dialogView.findViewById( R.id.on_duration_seekBar );
		onDurationSeekBar.setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener(){
			@Override
			public void onProgressChanged( SeekBar seekBar , int progress , boolean fromUser ){
				Log.i( TAG , "onProgressChanged" );
				onDurationIndicator.setText( getString( R.string.ms , progress + Utils.ADDITIONAL_DELAY_VALUE ) );
			}
			
			@Override
			public void onStartTrackingTouch( SeekBar seekBar ){
			
			}
			
			@Override
			public void onStopTrackingTouch( SeekBar seekBar ){
			
			}
		} );
		onDurationSeekBar.setProgress( utils.getInt( Utils.ON_TIME , Utils.DEFAULT_DELAY_VALUE ) - Utils.ADDITIONAL_DELAY_VALUE );
		SeekBar offDurationSeekBar = dialogView.findViewById( R.id.off_duration_seekBar );
		offDurationSeekBar.setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener(){
			@Override
			public void onProgressChanged( SeekBar seekBar , int progress , boolean fromUser ){
				offDurationIndicator.setText( getString( R.string.ms , progress + Utils.ADDITIONAL_DELAY_VALUE ) );
			}
			
			@Override
			public void onStartTrackingTouch( SeekBar seekBar ){
			
			}
			
			@Override
			public void onStopTrackingTouch( SeekBar seekBar ){
			
			}
		} );
		offDurationSeekBar.setProgress( utils.getInt( Utils.OFF_TIME , Utils.DEFAULT_DELAY_VALUE ) - Utils.ADDITIONAL_DELAY_VALUE );
		SeekBar numberSeekBar = dialogView.findViewById( R.id.flash_number_seekBar );
		numberSeekBar.setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener(){
			@Override
			public void onProgressChanged( SeekBar seekBar , int progress , boolean fromUser ){
				numberIndicator.setText( getResources().getQuantityString( R.plurals.times , progress + Utils.ADDITIONAL_NUMBER_VALUE , progress + Utils.ADDITIONAL_NUMBER_VALUE ) );
			}
			
			@Override
			public void onStartTrackingTouch( SeekBar seekBar ){
			
			}
			
			@Override
			public void onStopTrackingTouch( SeekBar seekBar ){
			
			}
		} );
		numberSeekBar.setProgress( utils.getInt( Utils.FLASH_NUMBER , Utils.DEFAULT_FLASH_NUMBER_VALUE ) - Utils.ADDITIONAL_NUMBER_VALUE );
		//-----------------------------------------------------------------------------------------
		builder.setView( dialogView );
		builder.setNegativeButton( R.string.cancel , ( dialog , which ) -> {
			dialog.cancel();
		} );
		builder.setPositiveButton( R.string.save , ( dialog , which ) -> {
			utils.putBoolean( Utils.MAIN_FLASH_ACTIVE , mainFlash.isChecked() );
			utils.putBoolean( Utils.FRONT_FLASH_ACTIVE , frontFlash.isChecked() );
			utils.putInt( Utils.ON_TIME , onDurationSeekBar.getProgress() + Utils.ADDITIONAL_DELAY_VALUE );
			utils.putInt( Utils.OFF_TIME , offDurationSeekBar.getProgress() + Utils.ADDITIONAL_DELAY_VALUE );
			utils.putInt( Utils.FLASH_NUMBER , numberSeekBar.getProgress() + Utils.ADDITIONAL_NUMBER_VALUE );
		} );
		builder.setNeutralButton( R.string.test , ( dialog , which ) -> Log.i( TAG , "flashSettingsOnClick: test" ) );
		AlertDialog dialog = builder.create();
		dialog.show();
		dialog.getButton( androidx.appcompat.app.AlertDialog.BUTTON_NEUTRAL ).setOnClickListener( v -> new TestFlasher( onDurationSeekBar.getProgress() + Utils.ADDITIONAL_DELAY_VALUE , offDurationSeekBar.getProgress() + Utils.ADDITIONAL_DELAY_VALUE , numberSeekBar.getProgress() + Utils.ADDITIONAL_NUMBER_VALUE , this ).start() );
	}
	
	public void missedSettingsOnClick( View view ){
		AlertDialog.Builder builder = new AlertDialog.Builder( this );
		builder.setPositiveButton( R.string.save , ( dialog , which ) -> {
			dialog.cancel();
		} );
		builder.setTitle( R.string.missed );
		View dialogView = getLayoutInflater().inflate( R.layout.missed_settings_dialog , null );
		builder.setView( dialogView );
		builder.setNegativeButton( R.string.cancel , ( dialog , which ) -> {
			dialog.cancel();
		} );
		builder.create().show();
	}
	
	//-------------------------------------------------DIALOGS------------------------------------------------------------
	
	private void showFlashAlert(){
		new AlertDialog.Builder( this , R.style.Theme_StrobeOnCallTheme_Dialog ).setCancelable( false ).setPositiveButton( "OK" , ( dialog , which ) -> dialog.cancel() ).setView( getLayoutInflater().inflate( R.layout.flash_alert_dialog , null ) ).create().show();
	}
	
	private void showCallPermissionDialog(){
		callPermissionDialog = new AlertDialog.Builder( this , R.style.Theme_StrobeOnCallTheme_Dialog ).setMessage( R.string.call_permission_explanation ).setPositiveButton( R.string.give , ( ( dialog , which ) -> {
			isFromCallPermissionDialog = true;
			ActivityCompat.requestPermissions( this , new String[]{ Manifest.permission.READ_PHONE_STATE } , 0 );
		} ) ).setNegativeButton( R.string.cancel , ( dialog , which ) -> dialog.cancel() ).create();
		callPermissionDialog.setOnCancelListener( dialog -> callSwitch.setChecked( false ) );
		callPermissionDialog.show();
	}
	
	public void showNotificationsPermissionDialog(){
		notificationPermissionDialog = new AlertDialog.Builder( this , R.style.Theme_StrobeOnCallTheme_Dialog ).setMessage( R.string.notifications_access_permission_explanation ).setNegativeButton( R.string.cancel , ( dialog , which ) -> dialog.cancel() ).setPositiveButton( R.string.give , ( dialog , which ) -> {
			startActivity( new Intent( Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS ) );
		} ).create();
		notificationPermissionDialog.setOnCancelListener( dialog -> {
			notificationSwitch.setChecked( false );
			smsSwitch.setChecked( false );
		} );
		notificationPermissionDialog.show();
	}
	
	//-------------------------------Other functions-------------------------------------------------------------------
	
	void updateAppsNumberText(){
		int appsEnabled = utils.getInt( Utils.NOTIFICATIONS_APPS_NUMBER , - 1 );
		String text;
		switch( appsEnabled ){
			case - 1:
				text = getString( R.string.all_apps_enabled );
				break;
			case 0:
				text = getString( R.string.no_apps_enabled );
				break;
			default:
				text = getString( R.string.enabled_apps , getResources().getQuantityString( R.plurals.apps , appsEnabled , appsEnabled ) );
		}
		appsListInfo.setText( text );
	}
	
	void updateWorkingModesInfoText(){
		StringBuilder builder = new StringBuilder();
		if( utils.getBoolean( Utils.NORMAL_MODE , true ) ){
			builder.append( getString( R.string.normal_mode ).toLowerCase() );
		}
		if( utils.getBoolean( Utils.VIBRATE_MODE , true ) ){
			if( builder.length() > 0 ) builder.append( ", " );
			builder.append( getString( R.string.vibrate_mode ).toLowerCase() );
		}
		if( utils.getBoolean( Utils.SILENT_MODE , false ) ){
			if( builder.length() > 0 ) builder.append( ", " );
			builder.append( getString( R.string.silent_mode ).toLowerCase() );
		}
		if( builder.length() > 0 ) builder.append( ", " );
		builder.append( getString( R.string.percents , utils.getInt( Utils.BATTERY_LEVEL , 25 ) ) );
		workingModesInfo.setText( builder.toString() );
	}
}
